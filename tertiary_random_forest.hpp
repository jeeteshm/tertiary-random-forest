#pragma once 

#include "utils.hpp"
#include<assert.h>
#include<limits.h>
#define DEBUG 1
using namespace std;

namespace AlgoComp {
  int num_predictors_;
  class Node{
    int first_child_, second_child_, third_child_;
    int indicator_index_;
    double value_1_, value_2_;
    char terminal_;
    double prediction_;
    
    public:
    Node(int first_child_, int second_child_, int third_child_,
          int indicator_index_, double value_1_, double value_2_,
          char terminal_, double prediction_){
      this->first_child_ = first_child_;
      this->second_child_ = second_child_;
      this->third_child_ = third_child_;
      this->indicator_index_ = indicator_index_;
      this->value_1_ = value_1_;
      this->value_2_ = value_2_;
      this->terminal_ = terminal_;
      this->prediction_ = prediction_;
    }

    void PrintNode(){
      cout<<"NODE "<<first_child_<<" "<<second_child_<<" "<<third_child_
        <<" "<<indicator_index_<<" "<<value_1_<<" "<<value_2_<<" "
        <<terminal_<<" "<<prediction_<<endl;
    }
    
    bool IsTerminal(){
      return this->terminal_ == 'Y';
    }
    
    double GetPrediction(){
      assert(this->IsTerminal());
      if(DEBUG){
        cout<<"Predicting "<<this->prediction_<<" for ";
        PrintNode();
        cout<<endl;
      }
      return this->prediction_;
    }
    
    int GetInputIndex(){
      return this->indicator_index_;
    }
    int GetNextNode(int input_index_, double input_value_){
      assert(!this->IsTerminal());
      assert(this->indicator_index_ == input_index_);
      if(input_value_ < this->value_1_){
        return this->first_child_;
      } else if (input_value_ > this->value_2_) {
        return this->third_child_;
      } else {
        return this->second_child_;
      }
    }
    
  };
  
  class TertiaryRandomTree {
    int tree_index_;
    vector<Node> nodes_;
    vector<int> first_node_on_last_path_;
    vector<double> last_input_values_;
    double last_prediction_;
    
    public:
    void SetTreeIndex(int tree_index_){
      this->tree_index_ = tree_index_;
    }
    
    void InsertChild(Node *node){
      this->nodes_.push_back(*node);
    }
    
    void PrintTree(){
      cout<<"Printing tree at index "<<tree_index_<<endl;
      for(vector<Node>::iterator it = nodes_.begin();
            it!=nodes_.end(); it++){
        (*it).PrintNode();
        cout<<endl;
      }
    }
    
    double InitializeTree(){
      for(int i =0; i<num_predictors_; i++){
        first_node_on_last_path_.push_back(INT_MAX);
        last_input_values_.push_back(0.0);
      }
      this->last_prediction_ = 0.0;
      int root_node_index_ = 1;
      Node root_node_ = this->nodes_[root_node_index_];
      
      this->first_node_on_last_path_[root_node_.GetInputIndex()]
        = root_node_index_;
      
      double current_prediction_ = OnInputDelta(root_node_.GetInputIndex(),
        0.0);
      return current_prediction_;
    }
    
    double OnInputDelta(unsigned int input_index_, double input_value_){
      if(DEBUG){
        cout<<"OnInputDelta( "<<input_index_<<", "<<input_value_
          <<")"<<endl;
      }
      this->last_input_values_[input_index_] = input_value_;
      int current_node_index_ = first_node_on_last_path_[input_index_];

      if(DEBUG){
        cout<<"current_node_index_ = "<<current_node_index_<<endl;
      }
      
      if(current_node_index_ == INT_MAX){
        return 0.0;
      }
      
      Node current_node_ = nodes_[current_node_index_];

      for(int i = 0; i<num_predictors_; i++){
        if(first_node_on_last_path_[i] > current_node_index_){
          first_node_on_last_path_[i] = INT_MAX;
        }
      }
        
      while(1){

        Node current_node_ = this->nodes_[current_node_index_];
        if(first_node_on_last_path_[current_node_.GetInputIndex()] > current_node_index_){
          first_node_on_last_path_[current_node_.GetInputIndex()] = current_node_index_;
        }

        if(current_node_.IsTerminal()){

          double current_prediction_ = current_node_.GetPrediction();
          double delta_ = current_prediction_ - this->last_prediction_;
          this->last_prediction_ = current_prediction_;
          if(DEBUG){
            cout<<"prediction: "<<current_prediction_
              <<"last prediction: "<<last_prediction_
              <<"delta: "<<delta_<<endl;
          }
          return delta_;
          
        }
        
        int next_node_index_ = current_node_.GetNextNode(
          current_node_.GetInputIndex(),
          this->last_input_values_[current_node_.GetInputIndex()]
        );
        
        current_node_index_ = next_node_index_;
        
        if(DEBUG){
          cout<<"next_node_index_: "<<next_node_index_<<endl;
        }
      }
     }
  };


  class InputChangeListener {
    public:
    virtual ~InputChangeListener ( ) { };

    virtual double OnInputChange ( unsigned int input_index_, double input_value_ ) = 0;
  };

  class TertiaryRandomForest : public InputChangeListener {
    vector<TertiaryRandomTree> trees_; 
    double sum_predictions_;
    int num_trees_;
    
    vector<string> split(string str) {
      vector<string> answer;
      int j=0;
      for(int i=0;i<str.length();i++) {
        if(str[i] == ' ') {
          answer.push_back(str.substr(j, i-j));
          j=i+1;
        }
      }
      answer.push_back(str.substr(j, str.length()-j));
      return answer;
    }
    
    public:
    TertiaryRandomForest ( ) { }
    
    void InitializeForest ( const char * const forest_filename_ ) {
      this->num_trees_ = 0;
      this->sum_predictions_ = 0.0;
      
      std::ifstream forest_file_;
      forest_file_.open ( forest_filename_, std::ifstream::in ) ;
      std::string line;
      getline(forest_file_, line);
      vector<string> tokens_ = split(line);
      num_predictors_ = atoi(tokens_[1].c_str()); //NUM_PREDICTORS 27
      
      getline(forest_file_, line);
      tokens_ = split(line);
      
      while(!forest_file_.eof() && tokens_[0].compare("TREESTART")==0){
        int tree_index_ = atoi(tokens_[1].c_str());
        TertiaryRandomTree *newTree = new TertiaryRandomTree();
        newTree->SetTreeIndex(tree_index_);
        //~ Node *newNode = new Node(0,0,0,0,0,0,'N',0);
        //~ newTree->InsertChild(newNode);


        getline(forest_file_, line);
        tokens_ = split(line);
        while(!forest_file_.eof() && tokens_[0].compare("TREELINE")==0){
            
            int first_child_ = atoi(tokens_[1].c_str()),
              second_child_ = atoi(tokens_[2].c_str()),
              third_child_ = atoi(tokens_[3].c_str());
            int indicator_index_ = atoi(tokens_[4].c_str());
            double value_1_ = strtod(tokens_[5].c_str(), NULL),
              value_2_ = strtod(tokens_[6].c_str(), NULL);
            char terminal_ = tokens_[7].at(0);
            double prediction_ = strtod(tokens_[8].c_str(), NULL);
            Node *newNode = new Node(first_child_, second_child_,
              third_child_, indicator_index_, value_1_, value_2_,
              terminal_, prediction_);
            newTree->InsertChild(newNode);
            getline(forest_file_, line);
            tokens_ = split(line);
        }
        
        this->trees_.push_back(*newTree);
        this->sum_predictions_ += newTree->InitializeTree();
        this->num_trees_ ++;
      }
    } /// needs to be implemented
 
    void PrintForest(){
      cout<<"#Predictors "<<num_predictors_<<endl;
      for(vector<TertiaryRandomTree>::iterator it = trees_.begin();
            it!=trees_.end(); it++){
        (*it).PrintTree();
      }
      cout<<"DONE PRINTING FOREST"<<endl;
    }
    
    double OnInputChange ( unsigned int input_index_, double input_value_ ) {
      double delta_=0.0;
      cout<<"delta: "<<delta_<<endl;
      for(int i=0; i<this->num_trees_; i++){
        delta_ += this->trees_[i].OnInputDelta(input_index_, input_value_);
        cout<<"delta: "<<delta_<<endl;
      }
      
      this->sum_predictions_ += delta_;
      return this->sum_predictions_ / this->num_trees_;

    } /// needs to be implemented
      
  };

}
